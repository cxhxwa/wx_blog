//获取应用实例
const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
var pageNo = 1;
Page({
  data: {
    current: 0, //当前所在滑块的 index
    scrollLeft: 0, //滚动条的位置,一个选项卡宽度是90（自定义来自css），按比例90*n设置位置
    navlist: ["全部", "NodeJS", "Nginx", "前端", "数据库"],
    content: [],
    tip: "",
    loading: false,
		userInfo: {},
    isShowModal: true,
    user_comment: '',
		isShow: true
  },

  cancel: function () {
    this.setData({
      isShowModal: !this.data.isShowModal
    });
  },

  commentInput: function (e) {
    this.setData({
      user_comment: e.detail.value
    });
  },

  showModal: function() {
    this.setData({
      isShowModal: false
    });
  },

  confirm: function () {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/addUserApply',
      method: "post",
      data: {
        myphone: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid,
        openid: that.data.authorInfo.author_openid
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
            isShowModal: !that.data.isShowModal,
            user_comment: ''
          });
          $Toast({
            content: res.data.payload,
            type: 'success'
          });
        } else {
          that.setData({
            isShowModal: !that.data.isShowModal,
            user_comment: ''
          });
          $Toast({
            content: res.data.err,
            type: 'error'
          });
        }
      }
    });
  },

  //tab切换
	tab: function (event) {
		//通过cateUrl去查询自己发表的cateUrl类型的数据
		var cateUrl = this.data.navlist[event.target.dataset.current].labels;
		pageNo = 1;
		var that = this;
		this.setData({
			current: event.target.dataset.current,
			content: []
		});

		wx.request({
			url: util.basePath + '/article/v1/userDesignArticleLabels',
			method: "post",
			data: {
				myphone: that.data.userInfo.account,
				app_sid: that.data.userInfo.app_sid,
				openid: that.data.authorInfo.author_openid,
				article_id: that.data.article_id,
				cateUrl: cateUrl,
				page: pageNo
			},
			header: {
				'content-type': 'application/json'
			},
			success(res) {
				console.log(res);
				if (res.data.status == 200) {
					that.setData({ content: that.data.content.concat(res.data.payload) });
				} else {
					$Toast({
						content: res.data.err,
						type: 'error'
					});
					that.setData({
						content: [],
						isShow: false
					});
				}
			}
		});
	},

  onLoad: function(options) {
    var that = this;

		var id = options.id;
		var userInfo = wx.getStorageSync("loginInfo");
		var authorInfo = wx.getStorageSync("authorInfo");
		that.setData({ userInfo: userInfo, authorInfo: authorInfo, article_id: id});

		that.getUserDesignArticle();
  },

  onPullDownRefresh: function() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    this.data.content = [];
    pageNo = 1;
		this.getUserDesignArticle();

    setTimeout(function() {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 1500);
  },

  onReachBottom: function() {
		++pageNo;
		this.getUserDesignArticle();
  },

  onShareAppMessage: function(res) {
		
  },

  details(e) {
    //详情页跳转
    wx.navigateTo({
      url: '../details/details?id=' + e.currentTarget.id
    })
  },
	getUserDesignArticle: function() {
    var that = this;
    that.setData({
      loading: true,
      tip: "正在加载"
    });

    wx.request({
			url: util.basePath + '/article/v1/userDesignArticle',
      method: "post",
      data: {
        myphone: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid,
				openid: that.data.authorInfo.author_openid,
				article_id: that.data.article_id,
				page: pageNo
      },
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
						loading: false,
						tip: "没有数据了",
						content: that.data.content.concat(res.data.payload.data),
						navlist: res.data.payload.labels,
						isFriend: res.data.payload.isFriend
          });
        } else {
					that.setData({
						loading: false,
						tip: "没有数据了"
					});
        }
      },
			
			fail(err) {
				console.log(err)
			}
    });
  }
});