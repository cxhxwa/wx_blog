// pages/list/list.js
const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
var pageNo = 1;
var cateUrl = '';
Page({
  data: {
    tip: "",
    loading: false,
    content: []
  },

  onLoad: function(options) {
    cateUrl = options.cateUrl;
    wx.setNavigationBarTitle({
      title: options.cateUrl
    });
		pageNo = 1;

    this.getIndexList();
  },

  details(e) {
    wx.navigateTo({
      url: '../details/details?id=' + e.currentTarget.id
    })

  },
	
  onReachBottom: function() {
    var that = this;
    ++pageNo;
    this.getIndexList();
  },
 
  onPullDownRefresh: function() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    this.data.content = [];
    pageNo = 1;
    this.getIndexList();

    setTimeout(function() {
      // 隐藏导航栏加载框
      wx.hideNavigationBarLoading();
      //停止当前页面下拉刷新。
      wx.stopPullDownRefresh()
    }, 1500)
  },
	
  onShareAppMessage: function() {
    return {
      title: '空城丶Blog',
      path: '/pages/index/index'
    }
  },
  getIndexList: function() {
    var that = this;
    that.setData({
      loading: true,
      tip: "正在加载"
    });

    //获取分类文章列表
    var that = this;
    wx.request({
      url: util.basePath + '/article/v1/keywordSearchArticle',
      method: "post",
      data: {
        page: pageNo,
        size: 10,
        cateUrl: cateUrl
      },
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
					console.log(res.data.payload);
          that.setData({ 'content': that.data.content.concat(res.data.payload) });
					that.setData({
						loading: false,
						tip: res.data.payload.length < 10 ? '数据已经到底了' : '下拉继续加载'
					});
        } else {
          that.setData({
            loading: false,
						tip: '数据已经到底了'
          });
          $Toast({
            content: '暂无该类型文章!',
            type: 'warn'
          });
        } 
      }
    });
  }
});