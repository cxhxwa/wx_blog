//获取应用实例
const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');
var pageNo = 1;
Page({
  data: {
    current: 0, //当前所在滑块的 index
    scrollLeft: 0, //滚动条的位置,一个选项卡宽度是90（自定义来自css），按比例90*n设置位置
    navlist: ["全部", "NodeJS", "Nginx", "前端", "数据库"],
    content: [],
    tip: "",
    loading: false,
		userInfo: {},
		isShow: true
  },

  //tab切换
	tab: function (event) {
		var cateUrl = this.data.navlist[event.target.dataset.current].labels;
		pageNo = 1;
		var that = this;
		this.setData({
			current: event.target.dataset.current,
			content: []
		});

		wx.request({
			url: util.basePath + '/article/v1/authorDesignArticleLabels',
			method: "post",
			data: {
				app_sid: that.data.chatInfo.friendInfo.app_sid,
				openid: that.data.chatInfo.friendInfo.openid,
				page: pageNo,
				cateUrl: cateUrl
			},
			header: {
				'content-type': 'application/json'
			},
			success(res) {
				if (res.data.status == 200) {
					that.setData({ content: that.data.content.concat(res.data.payload) });
				} else {
					$Toast({
						content: res.data.err,
						type: 'error'
					});
					that.setData({
						content: [],
						isShow: false
					});
				}
			}
		});
	},

  onLoad: function() {
    var that = this;

		//获取作者信息
		var chatInfo = wx.getStorageSync("chatInfo");

		if(!chatInfo) {
			$Toast({
				content: '请先登录!',
				type: 'warn'
			});
			wx.reLaunch({
				url: '/pages/userinfo/userinfo',
			});
		} else {
			that.setData({chatInfo: chatInfo});
			//获取作者创作文章数据
			this.getUserDesignArticle();
		}
  },

  onPullDownRefresh: function() {
    // 显示顶部刷新图标
    wx.showNavigationBarLoading();
    this.data.content = [];
    pageNo = 1;
		this.getUserDesignArticle();

    setTimeout(function() {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 1000);
  },

  onReachBottom: function() {
		++pageNo;
		this.getUserDesignArticle();
  },

  details(e) {
    //详情页跳转
    wx.navigateTo({
      url: '../details/details?id=' + e.currentTarget.id
    })
  },  

	getUserDesignArticle: function() {
    var that = this;

    that.setData({
      loading: true,
      tip: "正在加载"
    });

    wx.request({
			url: util.basePath + '/article/v1/authorDesignArticle',
      method: "post",
      data: {
				app_sid: that.data.chatInfo.friendInfo.app_sid,
				openid: that.data.chatInfo.friendInfo.openid,
				page: pageNo
      },
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          that.setData({
						loading: false,
						tip: "没有数据了",
						content: that.data.content.concat(res.data.payload.data),
						navlist: res.data.payload.labels
          });
        } else {
					that.setData({
						loading: false,
						tip: "没有数据了"
					});
        }
      }
    });
  },

	sendMessage: function() {
		wx.navigateTo({
			url: '/pages/messageboard/messageboard',
		});
	}
});