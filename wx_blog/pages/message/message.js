const app = getApp();
const util = require('../../utils/util.js');
const {
  $Toast
} = require('../../dist/base/index');

Page({
  data: {
    labels: ['好友申请', '评论', '点赞'],
    cateUrl: '好友申请',
    current: 0,
    message: [],
    applyStatus: true,
    commentStatus: false,
    starStatus: false,
		readChangeType: false,
		isShow: true
  },

  onLoad: function() {
    var userInfo = wx.getStorageSync("loginInfo");
		var unreadMessage = wx.getStorageSync("unreadMessage");
    this.setData({
      userInfo: userInfo,
			unreadMessage: [unreadMessage.add_count, unreadMessage.comment_count, unreadMessage.stars_count]
    });

    //调用查询消息数据api查询对应的消息数据
    this.getMessage();
  },

  onReady: function() {

  },

  onShow: function() {

  },

  onHide: function() {

  },

  onUnload: function() {
		wx.reLaunch({
			url: '/pages/userinfo/userinfo',
		});
  },

  onPullDownRefresh: function() {

  },

  onReachBottom: function() {

  },

  onShareAppMessage: function() {

  },
  //tab切换
  tab: function(event) {
    this.setData({
      cateUrl: this.data.labels[event.target.dataset.current],
      current: event.target.dataset.current,
      applyStatus: event.target.dataset.current == 0 ? true : false,
      commentStatus: event.target.dataset.current == 1 ? true : false,
      starStatus: event.target.dataset.current == 2 ? true : false
    });

		if (event.target.dataset.current != 0){
			this.setData({
				readChangeType: true
			});
		}

    //调用查询消息数据api查询对应的消息数据
    this.getMessage();
  },

  getMessage: function() {
    var that = this;

    //查询好友申请数据
    if (that.data.current == 0) {
      that.getUserMessage();
    }

    //查询评论消息
    if (that.data.current == 1) {
      that.getCommentMessage();
    }

    //查询点赞消息
    if (that.data.current == 2) {
      that.getStarMessage();
    }
  },

	readChangeType: function() {
		if (this.data.current == 1) {
			this.changeCommentType(this);
		}

		if (this.data.current == 2) {
			this.changeStarsType(this);
		}
	},

	changeCommentType: function(that) {
		wx.request({
			url: util.basePath + '/article/v1/changeCommentType',
			method: "post",
			data: {
				openid: that.data.userInfo.openid,
				app_sid: that.data.userInfo.app_sid
			},
			header: {
				'content-type': 'application/json'
			},
			success(res) {
				if (res.data.status == 200) {
					$Toast({
						content: res.data.payload,
						type: 'success'
					});

					that.setData({ ['unreadMessage[1]']: 0})
					wx.setStorage({
						key: 'unreadMessage',
						data: {
							add_count: that.data.unreadMessage[0],
							comment_count: that.data.unreadMessage[1],
							stars_count: that.data.unreadMessage[2]
						},
					});
				} else {
					$Toast({
						content: res.data.err,
						type: 'error'
					});
				}
			}
		});
	},

	changeStarsType: function (that) {
		wx.request({
			url: util.basePath + '/article/v1/changeStarsType',
			method: "post",
			data: {
				openid: that.data.userInfo.openid,
				app_sid: that.data.userInfo.app_sid
			},
			header: {
				'content-type': 'application/json'
			},
			success(res) {
				if (res.data.status == 200) {
					$Toast({
						content: res.data.payload,
						type: 'success'
					});

					that.setData({ ['unreadMessage[2]']: 0 });
					wx.setStorage({
						key: 'unreadMessage',
						data: {
							add_count: that.data.unreadMessage[0],
							comment_count: that.data.unreadMessage[1],
							stars_count: that.data.unreadMessage[2]
						},
					});
				} else {
					$Toast({
						content: res.data.err,
						type: 'error'
					});
				}
			}
		});
	},

  getUserMessage() {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/getUserMessage',
      method: "post",
      data: {
        account: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          var message = [];
          res.data.payload.forEach(data => {
            message.push({
              avatar: data.avatar,
              username: data.username,
              city: data.city,
              province: data.province,
              created_date: data.created_date,
              comment: data.comment,
              isSuccess: data.status == 2 || data.status == 3 ? true : false,
              apply_result: data.status == 2 ? "已同意好友申请" : "已拒绝好友申请",
              btn_type: data.status == 2 ? "primary" : "warn",
							id: data.id,
							myphone: data.friendphone,
							friendphone: data.myphone
            });
          });

          that.setData({
            message: message,
						isShow: false
          });
        }
      }
    });
  },

	consent_apply: function(e) {
		var that = this;

		var index = e.target.dataset.index;
		
		wx.request({
			url: util.basePath + '/article/v1/consentApply',
			method: "post",
			data: {
				id: that.data.message[e.target.dataset.index].id,
				myphone: that.data.message[e.target.dataset.index].myphone,
				friendphone: that.data.message[e.target.dataset.index].friendphone,
			},
			header: {
				'content-type': 'application/json'
			},
			success(res) {
				if (res.data.status == 200) {
					$Toast({
						content: '已同意好友申请!',
						type: 'success'
					});
					that.setData({ ['unreadMessage[0]']: that.data.unreadMessage[0] - 1 });
					wx.setStorage({
						key: 'unreadMessage',
						data: {
							add_count: that.data.unreadMessage[0],
							comment_count: that.data.unreadMessage[1],
							stars_count: that.data.unreadMessage[2]
						},
					});
					that.getMessage();
				} else {
					$Toast({
						content: res.data.err,
						type: 'error'
					});
				}
			}
		});
	},
	
	refuse_apply: function(e) {
		var that = this;

		var id = e.target.dataset.index;

		wx.request({
			url: util.basePath + '/article/v1/refuseApply',
			method: "post",
			data: {
				id: id
			},
			header: {
				'content-type': 'application/json'
			},
			success(res) {
				if (res.data.status == 200) {
					$Toast({
						content: '拒绝成功!',
						type: 'success'
					});
					that.getMessage();
				} else {
					$Toast({
						content: res.data.err,
						type: 'error'
					});
				}
			}
		});
	},

  getCommentMessage() {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/getCommentMessage',
      method: "post",
      data: {
        account: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          var message = [];
          res.data.payload.forEach(data => {
            message.push({
              avatar: data.avatar,
              username: data.username,
              city: data.city,
              province: data.province,
              created_date: data.created_date,
              comment: data.comment,
              article_id: data.article_id
            });
          });

          that.setData({
						message: message,
						isShow: false
          });
        }
      }
    });
  },

  getStarMessage() {
    var that = this;

    wx.request({
      url: util.basePath + '/article/v1/getStarMessage',
      method: "post",
      data: {
        account: that.data.userInfo.account,
        app_sid: that.data.userInfo.app_sid
      },
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        if (res.data.status == 200) {
          var message = [];
          res.data.payload.forEach(data => {
            message.push({
              avatar: data.avatar,
              username: data.username,
              city: data.city,
              province: data.province,
              created_date: data.created_date,
              article_id: data.article_id
            });
          });

          that.setData({
						message: message,
						isShow: false
          });
        }
      }
    });
  }
});