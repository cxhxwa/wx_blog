var common = module.exports;
var request = require('request');
var crypto = require('crypto');

common.baseRequest = function (url, param, callback) {
    url += '?';
    for (var key in param) {
        if (!param[key]) {
            continue;
        }
        url += key + '=' + param[key] + '&';
    }
    
    url = url_encode(url.substr(0, url.length - 1));

    var options = {
        url: url,
        method: 'GET'
    };

    request(options, function (error, response, body) {
        if (error && response && response.statusCode != 200 && !body) {
            callback('1服务器异常，请稍后重试');
            return;
        } else {
            if (body == 'success') {
                callback(null, body);
                return;
            }
            if (body && typeof body != 'string') {
                callback(null, body);
                return;
            }
            try {
                body = JSON.parse(body);
                callback(null, body);
            } catch (e) {
                callback(e);
            }
        }
    });
}

common.upload_img = function (par, cb) {
	if (!par.file) {
		cb('file参数缺失!');
	}

	cb(null, 0, par);
}

common.paramAll = function (req) {
    var parameter = {};
    if (req.params) {
        for (var p in req.params) {
            parameter[p] = req.params[p];
        }
    }
    if (req.body) {
        for (var p in req.body) {
            parameter[p] = req.body[p];
        }
    }
    if (req.query) {
        for (var p in req.query) {
            parameter[p] = req.query[p];
        }
    }
    return parameter;
};

function url_encode(url) {
    url = encodeURIComponent(url);
    url = url.replace(/\%3A/g, ":");
    url = url.replace(/\%2F/g, "/");
    url = url.replace(/\%3F/g, "?");
    url = url.replace(/\%3D/g, "=");
    url = url.replace(/\%26/g, "&");

    return url;
}

common.getRandomCode = function (length) {
    if (length > 0) {
       var data = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
       var nums = "";
       for (var i = 0; i < length; i++) {
          var r = parseInt(Math.random() * 61);
          nums += data[r];
       }
       return nums;
    } else {
       return false;
    }
 }

 common.encryPassword = function (password) {
    password = 'tao1024' + password;
    var md5 = crypto.createHash('md5');
    md5.update(password);
    password = md5.digest('hex');
    return password;
};

common.randomString = function (len) {
    len = len || 32;
    var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
    /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    var maxPos = $chars.length;
    var pwd = '';
    for (var i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
};