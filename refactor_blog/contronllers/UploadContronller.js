var express = require('express');
var router = express.Router();
var common = require('../util/common');
var uploadPicHelper = require('../util/uploadPicHelper');
require('../util/bootloader');

router.post('/upload_img', function(req, res) {
    uploadPicHelper.uploadPicsAndCheckPars(req, common.upload_img, 1, 'upload_img', true, function (err, errCode, par) {
        if (err) {
            return res.json(new ERR(err, errCode));
        } 
        
        return res.json(new PKG(par));
    });
});

module.exports = router; 