(function () {
    var CLOG = this.CLOG = function (msg, params) {
        if (params) {
            console.log(msg, params);
        } else {
            console.log(msg);
        }
    }

    var resp = this.Resp = function (err, status, payload) {
        this.err = err;
        this.status = status;
        this.payload = payload;
    }

    var req = this.Req = function (res) {
        var params = (res && res.params) ? res.params : {};
        var query = (res && res.query) ? res.query : {};
        this.uid = params.uid;
        this.ts = params.ts;
        this.query = query;
    }

    var PKG = this.PKG = function (payload) {
        this.status = 200;
        this.payload = remove_empty(payload);
    }

    var ERR = this.ERR = function (msg, status) {
        this.status = status ? status : 500;
        this.err = msg;
    }

    var remove_empty = function (target) {
        if (!(target instanceof Object)) return target;
        Object.keys(target).map(function (key) {
            if (target[key] instanceof Object) {
                if (!Object.keys(target[key]).length && typeof target[key].getMonth !== 'function') {
                    delete target[key];
                } else {
                    remove_empty(target[key]);
                }
            } else if (target[key] === null) {
                delete target[key];
            }
        });
        return target;
    };
})();