/**
 * Created by apple on 2017/7/6.
 */

var picHelp = module.exports;


var fs = require('fs');
var multiparty = require('multiparty');
var path = require('path');
var CONFIG = require('../config.js');
var https = require('https');
var http = require('http');
var imageInfo = require('imageinfo');
var url = require('url');
var mainPath = CONFIG.PICSMAINPATH;

// //上传文件数组，索引，参数，更改主路径,日期  递归调用修改
// function changeDir(par,index,userPath,callback) {
//
//     var files = par.files;
//     var uploadedPath = '/' + files[index].path;
//
//     //参数正确   更换图片路径
//     var picPach = userPath + index + '.jpg';
//     var dstPath = mainPath + picPach;
//
//     //重命名为真实文件名
//     fs.rename(uploadedPath,dstPath,function (err) {
//
//         if(err){
//             callback(err,par);
//             return;
//         }
//
//         par[('img' + (index + 1))] = picPach;
//         if(index < (files.length-1)){
//             changeDir(par,index+1,userPath,callback);
//         }else {
//             callback(null,par);
//         }
//
//     });
//
// }

//更新图片路径
function changeDir(par,index,userPath,callback) {
    var keyArr = Object.keys(par.files);

    if(keyArr.length < 1){
        callback(null,par);
        return;
    }
    var picObj = par.files[keyArr[index]][0];
    var uploadedPath = picObj.path;

    fs.readFile(uploadedPath, function (err,bytesRead) {
        if (err) {
            callback(err,par);
            return;
        }
        var info = imageInfo(bytesRead);
        var type;
        if(!info || !info.format){
            type = '.jpg';
        }else {
            type = imageInfoFileType(info.format);
            if (!type) {
                callback('上传图片格式有误', par);
                return;
            }
        }
        //参数正确   更换图片路径
        var picPach = userPath + type;
        var dstPath = mainPath + picPach;
        checkDirs(dstPath,function (exits) {
            if(exits){
                picPach = userPath + (keyArr.length + index) + type;
                dstPath = mainPath + picPach;
            }
            //重命名为真实文件名
            fs.rename(uploadedPath,dstPath,function (err) {
                if(err){
                    callback(err,par);
                    return;
                }
                par[picObj.fieldName] = picPach;
                if(index < (keyArr.length-1)){
                    changeDir(par,index+1,userPath,callback);
                }else {
                    callback(null,par);
                }
            });
        });
    });
}




//递归创建目录 异步方法
function mkdirs(dirname, callback) {
    fs.exists(dirname, function (exists) {
        if (exists) {
            callback(null);
        } else {
            mkdirs(path.dirname(dirname), function () {
                fs.mkdir(dirname, callback);
            });
        }
    });
}

//检查文件是否存在
function checkDirs(dirname,callback) {
    fs.exists(dirname, function (exists) {
        if (exists) {
            callback('存在');
        } else {
            callback(null);
        }
    });
}



// //上传图片并检验参数，带上检验参数的方法,验证不正确会返回错误并删除图片  用于首次上传图片
// //至少上传一张，命名为file
// //req 表单请求，
// // checkParFunc 检验参数的方法,构造好传进来 参数格式是(par,function (err,errCode,userInfo)) 在外面写时，如果有验证token,要把userInfo要传给par
// // maxPic 图片最大上传限制
// // cb回调
// //回调出去的par，files 是存放文件的数组 userInfo是用户信息
// picHelp.uploadPicAndCheckPar = function (req,checkParFunc,maxPic,pathDir,cb) {
//
//     if(!req || !checkParFunc || !pathDir){
//         cb('服务器异常',400);
//         console.log('调用uploadPicAndCheckPar方法参数有误')
//         return;
//     }
//     //生成multiparty对象，并配置上传目标路径
//
//     var form = new multiparty.Form({uploadDir: (mainPath + '/picTemp/')});
//     //上传完成后处理
//     form.parse(req,function (error,fields,files) {
//
//         if(error){
//             cb(error,400);
//             return;
//         }
//
//         console.log(files);
//
//         if(!files.file){
//             cb('参数有误',400);
//             return;
//         }
//
//         //查看图片是否超过限制
//         var picNum = 0;
//         for(var picKey in files){
//             picNum += files[picKey].length;
//         }
//         if(picNum > maxPic){
//             //图片超过限制，删除上传来的图片
//             delPicsWithFiles(files);
//             cb('图片个数超过限制',400);
//             return;
//         }
//
//         //参数格式转换,解析出来
//         var par = {};
//         for(var p in fields){
//             if(p[0]){
//                 par[p] = fields[p][0];
//             }
//         }
//
//         //检验参数是否正确,不正确的话去删除上传的图片，并且返回错误
//         checkParFunc(par,function (err,errCode) {
//
//             if(!err){//验证正确，去重命名
//
//                 par.files = files.file;
//
//                 picHelp.renamePics(par,pathDir,true,function (err,errCode,param) {
//
//                     if(err){
//                         cb(err,errCode);
//                         return;
//                     }
//                     cb(null,0,param);
//
//                 });
//
//
//                 return;
//             }
//
//             //验证不正确，删除上传来的图片
//             delPicsWithFiles(files);
//             cb(err,errCode);
//
//         });
//
//     });
//
// }
//
//
//
// //上传图片并检验参数，带上检验参数的方法,验证不正确会返回错误并删除图片  用于修改图片
// //req 表单请求，
// // checkParFunc 检验参数的方法,构造好传进来 参数格式是(par,function (err,errCode,userInfo))
// // maxPic 图片最大上传限制
// // cb回调
// //回调出去的par，files 是存放文件的数组 userInfo是用户信息
// picHelp.updatePicAndCheckPar = function (req,checkParFunc,maxPic,pathDir,cb) {
//
//     if(!req || !checkParFunc || !pathDir){
//         cb('服务器异常',400);
//         console.log('调用uploadPicAndCheckPar方法参数有误')
//         return;
//     }
//     //生成multiparty对象，并配置上传目标路径
//
//     var form = new multiparty.Form({uploadDir: (mainPath + '/picTemp/')});
//     //上传完成后处理
//     form.parse(req,function (error,fields,files) {
//
//         if(error){
//             cb(error,400);
//             return;
//         }
//
//
//         //查看图片是否超过限制
//         var picNum = 0;
//         var correctKeyArr = [];
//         for(var picKey in files){
//             if(files[picKey].length > 1){//每个名字只能带一张图片
//                 delPicsWithFiles(files);
//                 cb('图片参数有误',400);
//                 return;
//             }
//             picNum += files[picKey].length;
//             correctKeyArr.push(('img' + picNum));
//         }
//         if(picNum > maxPic){
//             //图片超过限制，删除上传来的图片
//             delPicsWithFiles(files);
//             cb('图片个数超过限制',400);
//             return;
//         }
//
//
//         //图片命名有误也取消上传
//         var picKeyArr = Object.keys(files);
//         for(var key in picKeyArr){
//             var i = 0;
//             for(;i<correctKeyArr.length; i++){
//                 if(picKeyArr[key] == correctKeyArr[i]){
//                     break;
//                 }
//             }
//             if(i == correctKeyArr.length){
//                 delPicsWithFiles(files);
//                 cb('图片参数有误',400);
//                 return;
//             }
//         }
//
//         //参数格式转换,解析出来
//         var par = {};
//         for(var p in fields){
//             if(p[0]){
//                 par[p] = fields[p][0];
//             }
//         }
//
//
//         //检验参数是否正确,不正确的话去删除上传的图片，并且返回错误
//         checkParFunc(par,function (err,errCode) {
//
//             if(!err){//验证正确，去重命名
//
//                 par.files = files;
//
//                 picHelp.renamePics(par,pathDir,false,function (err,errCode,param) {
//
//                     if(err){
//                         cb(err,errCode,param);
//                         delPicsWithFiles(files);
//                         return;
//                     }
//                     cb(null,0,param);
//
//                 });
//
//                 return;
//             }
//
//             //验证不正确，删除上传来的图片
//             delPicsWithFiles(files);
//             cb(err,errCode);
//
//         });
//
//     });
//
// }

//上传图片并检验参数，带上检验参数的方法,验证不正确会返回错误并删除图片
//req 表单请求，
// checkParFunc 检验参数的方法,构造好传进来，包括检验图片参数的名字 参数格式是(par,function (err,errCode,userInfo))
// maxPic 图片最大上传限制
// cb回调
//回调出去的par，files 是存放文件的数组 userInfo是用户信息
picHelp.uploadPicsAndCheckPar = function (req,checkParFunc,maxPic,pathDir,isNeedUid,cb) {
    if(!req || !checkParFunc || !pathDir ){
        cb('服务器异常',500);
        console.log('调用uploadPicAndCheckPar方法参数有误');
        return;
    }
    var isNeedUid = isNeedUid;
    if(!isNeedUid){
        isNeedUid = false;
    }
    //生成multiparty对象，并配置上传目标路径

    var form = new multiparty.Form({uploadDir: (mainPath + '/picTemp/')});
    //上传完成后处理
    form.parse(req,function (error,fields,files) {

        if(error){
            cb(error,400);
            return;
        }

        //参数格式转换,解析出来
        var par = {};
        for(var p in fields){
            if(p[0]){
                par[p] = fields[p][0];
            }
        }
        //查看图片是否超过限制
        var picNum = 0;
        par.picNames = Object.keys(files);
        var picSizeArr = [];
        for(var picKey in files){

            if(files[picKey].length > 1){//每个名字只能带一张图片
                delPicsWithFiles(files);
                cb('图片参数有误',400);
                return;
            }
            par[picKey] = files[picKey][0].path;
            picNum += files[picKey].length;
            picSizeArr.push(parseInt(files[picKey][0].size));

        }
        if(par.mp4_url) {
            if(!files.mp4_url || !files.mp4_url[0] || !files.mp4_url[0].size || files.mp4_url[0].size == 0) {
                cb('视频上传时发生错误！',400);
                return;
            }
            //10485760  10m
            //3145728    3m
            if(files.mp4_url[0].size > 800000000) {
                fs.unlink(par.mp4_url, function () {});
                cb('视频文件过大',400);
                return;
            }
            delete files['mp4_url'];
            delPicsWithFiles(files);
            pathDir = 'bbs_mp4';
            checkParFunc(par,function (err,errCode) {
                if(err){
                    fs.unlink(par.mp4_url, function () {});
                    return cb(err,errCode);
                } else {
                    picHelp.renameVideo(par,pathDir,isNeedUid,function (err,errCode,param) {
                        console.log(param);
                        if(err){
                            cb(err,errCode,param);
                            fs.unlink(par.mp4_url, function () {});
                            return;
                        }
                        cb(null,0,param);
                    });
                    return;
                }
            });
        } else {
            if(picNum > maxPic){
                //图片超过限制，删除上传来的图片
                delPicsWithFiles(files);
                cb('图片个数超过限制',400);
                return;
            }
            picSizeArr.sort(function(a, b){
                return a - b;
            }).reverse();
            if(picSizeArr[0] > 400000000) {
                delPicsWithFiles(files);
                cb('图片过大，请重新选图！',400);
                return;
            }
            //检验参数是否正确,包括图片命名,不正确的话去删除上传的图片，并且返回错误
            checkParFunc(par,function (err,errCode) {

                if(!err){//验证正确，去重命名

                    par.files = files;

                    picHelp.renamePics(par,pathDir,isNeedUid,function (err,errCode,param) {

                        if(err){
                            cb(err,errCode,param);
                            delPicsWithFiles(files);
                            return;
                        }
                        cb(null,0,param);

                    });


                    return;
                }

                //验证不正确，删除上传来的图片
                delPicsWithFiles(files);
                cb(err,errCode);
            });
        }
    });

}

//根据上传来的files表单删除图片
function delPicsWithFiles(files) {
    //图片超过限制，删除上传来的图片
    for (var key in files) {
        files[key].forEach(function (picObj) {
            var uploadedPath = picObj.path;
            fs.unlink(uploadedPath, function () {
            });
        });
    }
}

//给上传的图片重命名 //par：参数 picType：路径名
picHelp.renamePics = function (par,picType,isNeedUid,cb) {

    if(!par.files){
        cb('参数有误',400);
        return;
    }

    //构造路径
    var uid = 0;
    if(par.userInfo){
        uid = par.userInfo.main_userInfo ? par.userInfo.main_userInfo.uid : par.userInfo.uid;
    }
    var date = new Date();
    var userPath = '/' + picType;
    userPath += '/' + date.getFullYear();
    userPath += '/' + (date.getMonth()+1);
    userPath += '/' + date.getDate();
    if(isNeedUid == true) {
        userPath += '/' + parseInt(uid / 100);
        userPath += '/' + uid;
    }
    mkdirs((mainPath + userPath),function (err) {//创建目录

        if(err){
            cb(err,400);
            return;
        }

        userPath += '/' + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();

        changeDir(par, 0, userPath, function (err, par) {

            if (err) {
                cb(err, 400,par);
                return;
            }
            cb(null, 0, par);
        });

    });

}


//根据命名好的子路径删除图片的方法  将存放在字段中的路径组成数组传进来。
picHelp.delPics = function (pics) {

    if(pics.length < 1 || !(Object.prototype.toString.call(pics) === '[object Array]')){
        return;
    }

    //删除数组中的图片
    pics.forEach(function (pic) {
        var picUrl = mainPath + pic;
        fs.unlink(picUrl,function () {});
    });

}

//将正确的图片名字作为数组传进来，再将有的名字作为数组传进来，比对命名是否不规范
picHelp.checkPicName = function (correntPicKeys,picNames) {
    //检查图片命名是否正确
    for(var i=0; i<picNames.length; i++) {
        if (correntPicKeys.indexOf(picNames[i]) < 0) {
            return '图片参数有误';
        }
    }
}



//检验图片是否是长宽750 或 800 的
picHelp.checkPicSizeIn750Or800 = function (imgUrl,cb) {
    picHelp.getPicSize(imgUrl,function (err,size) {
        if(err){
            cb(false);
            return;
        }
        if(size.width == 750 && size.height == 750){
            cb(true);
            return;
        }
        if(size.width == 800 && size.height == 800){
            cb(true);
            return;
        }
        cb(false);
    });
}


//根据http 或 https 的url地址获取图片长宽
picHelp.getPicSize = function (imgUrl,cb) {
    var options = url.parse(imgUrl);
    if(imgUrl.indexOf('https') >= 0){
        getPicSizeUsingHttps(options,cb);
        return;
    }else if(imgUrl.indexOf('http') >= 0){
        getPicSizeUsingHttp(options,cb);
        return;
    }
    cb('图片地址有误');
}


function getPicSizeUsingHttp (options,cb) {
    try {
        http.get(options, function (response) {
            var chunks = [];
            response.on('data', function (chunk) {
                chunks.push(chunk);
            }).on('end', function() {
                var buffer = Buffer.concat(chunks);
                var info = imageInfo(buffer);
                var size = {
                    width: info.width,
                    height: info.height
                }
                console.log(size);
                cb(null,size);
            });
        });
    }
    catch (e) {
        cb(e);
        console.log(e);
        return;
    }
}

function getPicSizeUsingHttps (options,cb) {
    try {
        https.get(options, function (response) {
            var chunks = [];
            response.on('data', function (chunk) {
                chunks.push(chunk);
            }).on('end', function() {
                var buffer = Buffer.concat(chunks);
                var info = imageInfo(buffer);
                var size = {
                    width: info.width,
                    height: info.height
                }
                cb(null,size);
            });
        });
    }
    catch (e) {
        cb(e);
        console.log(e);
        return;
    }
}

function imageInfoFileType(type) {
    switch (type){
        case 'PNG' :
            return '.png';
            break;
        case 'JPG' :
            return '.jpg';
            break;
        case 'GIF' :
            return '.gif';
            break;
        case 'SWF' :
            return '.SWF';
            break;
        default :
            return undefined;
    }
}


//视频上传
picHelp.renameVideo = function (par,picType,isNeedUid,cb) {
    var uid = 0;
    if(par.userInfo){
        uid = par.userInfo.main_userInfo ? par.userInfo.main_userInfo.uid : par.userInfo.uid;
    }
    var date = new Date();
    var userPath = '/' + picType;
    userPath += '/' + date.getFullYear();
    userPath += '/' + (date.getMonth()+1);
    userPath += '/' + date.getDate();
    if(isNeedUid == true) {
        userPath += '/' + parseInt(uid / 100);
        userPath += '/' + uid;
    }
    mkdirs((mainPath + userPath),function (err) {//创建目录
        if(err){
            cb(err,400);
            return;
        }
        userPath += '/' + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
        var uploadedPath = par.mp4_url;
        fs.readFile(uploadedPath, function (err) {
            if (err) {
                cb(err, 400);
                return;
            }
            var type = par.mp4_url.split('.')[1];
            var picPach = userPath + '.' + type;
            par.mp4_name = picPach;
            var dstPath = mainPath + picPach;
            checkDirs(dstPath,function (exits) {
                if(exits){
                    picPach = userPath + type;
                    dstPath = mainPath + picPach;
                }
                fs.rename(uploadedPath,dstPath,function (err) {
                    if(err){
                        cb(err,400);
                        return;
                    }
                    par.mp4_url = par.mp4_name;
                    cb(null, 0, par);
                });
            });
        });
    });

}

/**
 * 方案一：
 * 1.上传图片检验图片参数，参数验证不正确返回error并删除图片
 * 2.设置图片上传数量上限，数量超过上限返回error并删除图片
 * 3.设置图片或视频大小，大小超过上限返回error并删除图片
 * 4.图片成功上传，回调到路由层进行数据库逻辑层处理
 */




//上传图片并检验参数，带上检验参数的方法,验证不正确会返回错误并删除图片
//req 表单请求，
// checkParFunc 检验参数的方法,构造好传进来，包括检验图片参数的名字 参数格式是(par,function (err,errCode,userInfo))
// maxPic 图片最大上传限制
// cb回调
//回调出去的par，files 是存放文件的数组,userInfo是用户信息
picHelp.uploadPicsAndCheckPars = function (req,checkParFunc,maxPic,pathDir,isNeedUid,cb) {
    if(!req || !checkParFunc || !pathDir ){
        cb('服务器异常',500);
        return;
    }
    var isNeedUid = isNeedUid;
    if(!isNeedUid){
        isNeedUid = false;
    }
    //生成multiparty对象，并配置上传目标路径

    var form = new multiparty.Form({uploadDir: (mainPath + '/picTemp/')});
    //上传完成后处理
    form.parse(req,function (error, fields, files) {
        
        if(error){
            cb(error,400);
            return;
        }

        //参数格式转换,解析出来
        var par = {};
        for(var p in fields){
            if(p[0]){
                par[p] = fields[p][0];
            }
        }
        //查看图片是否超过限制
        var picNum = 0;
        par.picNames = Object.keys(files);
        var picSizeArr = [];
        for(var picKey in files){

            if(files[picKey].length > 1){//每个名字只能带一张图片
                delPicsWithFiles(files);
                cb('图片参数有误',400);
                return;
            }
            par[picKey] = files[picKey][0].path;
            picNum += files[picKey].length;
            picSizeArr.push(parseInt(files[picKey][0].size));

        }
        if(par.mp4_url) {
            if(!files.mp4_url || !files.mp4_url[0] || !files.mp4_url[0].size || files.mp4_url[0].size == 0) {
                return cb('视频上传时发生错误！',400);
            }
            if(files.mp4_url[0].size > 8000000) {
                fs.unlink(par.mp4_url, function () {});
                return cb('视频文件过大',400);
            }

            delete files['mp4_url'];
            delPicsWithFiles(files);
            pathDir = 'bbs_mp4';

            checkParFunc(par,function (err,errCode) {
                if(err){
                    fs.unlink(par.mp4_url, function () {});
                    return cb(err,errCode);
                } 
                
                picHelp.renameVideo(par,pathDir,isNeedUid,function (err,errCode,param) {
                    if(err){
                        cb(err,errCode,param);
                        fs.unlink(par.mp4_url, function () {});
                        return;
                    }   

                    cb(null, 0, param);
                });
                return;
            });
        } else {
            if(picNum > maxPic){
                //图片超过限制，删除上传来的图片
                delPicsWithFiles(files);
                cb('图片个数超过限制',400);
                return;
            }
            picSizeArr.sort(function(a, b){
                return a - b;
            }).reverse();
            if(picSizeArr[0] > 4000000) {
                delPicsWithFiles(files);
                cb('图片过大，请重新选图！',400);
                return;
            }
            //检验参数是否正确,包括图片命名,不正确的话去删除上传的图片，并且返回错误
            checkParFunc(par,function (err,errCode) {

                if(!err){//验证正确，去重命名

                    par.files = files;

                    picHelp.renamePics(par,pathDir,isNeedUid,function (err,errCode,param) {

                        if(err){
                            cb(err,errCode,param);
                            delPicsWithFiles(files);
                            return;
                        }
                        cb(null,0,param);

                    });

                    return;
                }

                //验证不正确，删除上传来的图片
                delPicsWithFiles(files);
                cb(err,errCode);
            });
        }
    });

}





//根据上传来的files表单删除图片
function delPicsWithFiles(files) {
    //图片超过限制，删除上传来的图片
    for (var key in files) {
        files[key].forEach(function (picObj) {
            var uploadedPath = picObj.path;
            fs.unlink(uploadedPath, function () {
            });
        });
    }
}

//给上传的图片重命名 //par：参数 picType：路径名
picHelp.renamePics = function (par,picType,isNeedUid,cb) {
    if(!par.files){
        cb('参数有误',400);
        return;
    }
    //构造路径
    var uid = 0;
    if(par.userInfo){
        uid = par.userInfo.main_userInfo ? par.userInfo.main_userInfo.uid : par.userInfo.uid;
    }
    var date = new Date();
    var userPath = '/' + picType;
    userPath += '/' + date.getFullYear();
    userPath += '/' + (date.getMonth()+1);
    userPath += '/' + date.getDate();
    if(isNeedUid == true) {
        userPath += '/' + parseInt(uid / 100);
        userPath += '/' + uid;
    }
    mkdirs((mainPath + userPath),function (err) {//创建目录
        if(err){
            cb(err,400);
            return;
        }

        userPath += '/' + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
        changeDir(par, 0, userPath, function (err, par) {

            if (err) {
                cb(err, 400,par);
                return;
            }
            cb(null, 0, par);
        });
    });
}


//根据命名好的子路径删除图片的方法  将存放在字段中的路径组成数组传进来。
picHelp.delPics = function (pics) {

    if(pics.length < 1 || !(Object.prototype.toString.call(pics) === '[object Array]')){
        return;
    }

    //删除数组中的图片
    pics.forEach(function (pic) {
        var picUrl = mainPath + pic;
        fs.unlink(picUrl,function () {});
    });

}

//将正确的图片名字作为数组传进来，再将有的名字作为数组传进来，比对命名是否不规范
picHelp.checkPicName = function (correntPicKeys,picNames) {
    //检查图片命名是否正确
    for(var i=0; i<picNames.length; i++) {
        if (correntPicKeys.indexOf(picNames[i]) < 0) {
            return '图片参数有误';
        }
    }
}



//检验图片是否是长宽750 或 800 的
picHelp.checkPicSizeIn750Or800 = function (imgUrl,cb) {
    picHelp.getPicSize(imgUrl,function (err,size) {
        if(err){
            cb(false);
            return;
        }
        if(size.width == 750 && size.height == 750){
            cb(true);
            return;
        }
        if(size.width == 800 && size.height == 800){
            cb(true);
            return;
        }
        cb(false);
    });
}


//根据http 或 https 的url地址获取图片长宽
picHelp.getPicSize = function (imgUrl,cb) {
    var options = url.parse(imgUrl);
    if(imgUrl.indexOf('https') >= 0){
        getPicSizeUsingHttps(options,cb);
        return;
    }else if(imgUrl.indexOf('http') >= 0){
        getPicSizeUsingHttp(options,cb);
        return;
    }
    cb('图片地址有误');
}


function getPicSizeUsingHttp (options,cb) {
    try {
        http.get(options, function (response) {
            var chunks = [];
            response.on('data', function (chunk) {
                chunks.push(chunk);
            }).on('end', function() {
                var buffer = Buffer.concat(chunks);
                var info = imageInfo(buffer);
                var size = {
                    width: info.width,
                    height: info.height
                }
                console.log(size);
                cb(null,size);
            });
        });
    }
    catch (e) {
        cb(e);
        console.log(e);
        return;
    }
}

function getPicSizeUsingHttps (options,cb) {
    try {
        https.get(options, function (response) {
            var chunks = [];
            response.on('data', function (chunk) {
                chunks.push(chunk);
            }).on('end', function() {
                var buffer = Buffer.concat(chunks);
                var info = imageInfo(buffer);
                var size = {
                    width: info.width,
                    height: info.height
                }
                cb(null,size);
            });
        });
    }
    catch (e) {
        cb(e);
        console.log(e);
        return;
    }
}

function imageInfoFileType(type) {
    switch (type){
        case 'PNG' :
            return '.png';
            break;
        case 'JPG' :
            return '.jpg';
            break;
        case 'GIF' :
            return '.gif';
            break;
        case 'SWF' :
            return '.SWF';
            break;
        default :
            return undefined;
    }
}